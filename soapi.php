<?php

class SOAPI
{
    public $vars;
    public function __construct()
    {
        $this->vars = [];
    }

    public function storeId($id)
    {
        $this->vars['storeId'] = $id;
        return $this;
    }

    public function key($key)
    {
        $this->vars['apiKey'] = $key;
        return $this;
    }
    public function ver($ver)
    {
        $this->vars['ver'] = $ver;
        return $this;
    }

    public function baseUrl($url)
    {
        $this->vars['baseUrl'] = $url;
        return $this;
    }

    public function get($url)
    {
        if (isset($this->vars['baseUrl']) && !empty($this->vars['baseUrl'])) {
            $url = $this->vars['baseUrl'] . $url;
        }
        $token = $this->vars['storeId'] . ':' . $this->vars['apiKey'];
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_HTTPHEADER => array("Accept: application/json", "SO-Ver: {$this->vars['ver']}"),
            CURLOPT_USERPWD  => $token,
            CURLOPT_RETURNTRANSFER => true
            )
        );

        $output = curl_exec($ch);

        if ($output === false) {
            echo "cURL Error: " . curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }
}
